﻿namespace Cafe_App.Win10.Models
{
    public class Item
    {
        public int uid { get; set; }
        public string Name { get; set; }
        public string Descripcion { get; set; }
        public uint Amount { get; set; }

    }
}
