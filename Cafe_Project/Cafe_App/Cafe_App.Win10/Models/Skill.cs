﻿namespace Cafe_App.Win10.Models
{
    public class Skill
    {
        public int uid { get; set; }
        public string Name { get; set; }
        public string Descripcion { get; set; }
        public float Cooldown { get; set; }
        public int Cost { get; set; }
        public bool HITarea { get; set; }


    }
}
