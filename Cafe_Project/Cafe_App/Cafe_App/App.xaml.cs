﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Cafe_App.Services;
using Cafe_App.Views;

namespace Cafe_App
{
    public partial class App : Application
    {

        public App()
        {
            InitializeComponent();

            DependencyService.Register<MockDataStore>();
            MainPage = new AppShell();
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
